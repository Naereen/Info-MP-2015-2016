`Exercices sur la récursivité <../exos_rec.pdf>`_
=================================================
4ème TD, sur la récursivité.

.. automodule:: exos_rec
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_rec.py

Le fichier Python se trouve ici : :download:`exos_rec.py`.
