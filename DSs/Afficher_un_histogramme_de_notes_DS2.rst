Diagramme des notes (DS2)
=========================
Calculs de statistiques et affichage de deux graphiques pour le 2ème DS, algorithmique sur des tableaux 1D et 2D, et simulations probabilistes.

Quelques statistiques...
------------------------
- Pour ce contrôle, la moyenne est de **14.71/27**.
- Soit **11/20** (si on ne traffique *pas* les notes).
- La *meilleure* note est 23.75/27 soit **18/20**.
- La *moins bonne* note est 08.5/27 soit **06.5/20**.
- Pour ce contrôle, il y a **31 notes** (car ... 31 élèves !).


--------------------------------------------------------------------------------

Deux graphiques
---------------

Camembert :

.. image:: Camembert_notes_DS2.png
   :target: Afficher_un_histogramme_de_notes_DS2.html#camembert


Histogramme :

.. image:: Histogramme_notes_DS2.png
   :target: Afficher_un_histogramme_de_notes_DS2.html#histogramme


--------------------------------------------------------------------------------

Conseils
--------
- Améliorez la présentation de vos copies,
- Essayez d'être encore plus attentifs à la syntaxe de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants),
- Vous devez être plus attentifs aux consignes de l'énoncé (certains élèves oublient de donner la complexité dans les dernières questions),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller"* des points là où vous pouvez (la `Q09 <./X_PSI_PT__2015.html#X_PSI_PT__2015.coloriageAleatoire>`_ (:py:func:`X_PSI_PT__2015.coloriageAleatoire`) et `Q13 <./X_PSI_PT__2015.html#X_PSI_PT__2015.existeCheminSimple>`_ (:py:func:`X_PSI_PT__2015.existeCheminSimple`) étaient faciles, par exemple),
- Enfin, vous devez vous forcer à n'utiliser que les structures de données de l'énoncé (c'est flagrant, certains commencent par utiliser :py:func:`X_PSI_PT__2015.creerTableau` ou :py:func:`X_PSI_PT__2015.creerListeVide` au début, puis finissent avec des ``[]`` et :py:meth:`list.append()` à tout va), probablement par manque de concentration et à cause de la fatigue.


--------------------------------------------------------------------------------

Documentation
-------------

.. automodule:: Afficher_un_histogramme_de_notes_DS2
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python Afficher_un_histogramme_de_notes_DS2.py

Le fichier Python se trouve ici : :download:`Afficher_un_histogramme_de_notes_DS2.py`.
