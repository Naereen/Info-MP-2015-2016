Diagramme des notes (DS4)
=========================
Calculs de statistiques et affichage de deux graphiques pour le 4ème DS, portant sur l'étude de bout en bout d'un problème d'ingénierie numérique réelle (installation d'un réseau connecté de fissuromètre).

Quelques statistiques...
------------------------
- Pour ce contrôle, la moyenne est de **12.95/25**, soit **10.31/20**.
- L'écart-type était de **3.9/25**, environ **3.5** sur 20; le niveau de la classe est assez étalé.
- La *meilleure* note est 20.75/25 soit **18/20**.
- La *moins bonne* note est 06.5/25 soit **05.7/20**.
- Pour ce contrôle, il y a **31 notes** (car ... 31 élèves !).


--------------------------------------------------------------------------------

Deux graphiques
---------------

Camembert :

.. image:: Camembert_notes_DS4.png
   :target: Afficher_un_histogramme_de_notes_DS4.html#camembert


Histogramme :

.. image:: Histogramme_notes_DS4.png
   :target: Afficher_un_histogramme_de_notes_DS4.html#histogramme


--------------------------------------------------------------------------------

Conseils
--------
Des conseils un peu en vrac :

- *Améliorez la présentation* de vos copies, encore et toujours ! *Souligner* est une bonne idée, mais le faire à l'arrache sans règle ne vous aidera pas. *Utiliser des couleurs* est un plus, mais restez sobres (vert foncé ou rouge, vert étant préféré). Mettez toujours en valeur vos résultats (souligner ou encadrer, mais encadrer est plus long),
- *Inutile de recopier* l'énoncé ou les titres, écrire ``III`` suffit.
- Essayez d'être encore plus *attentif à la syntaxe* de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants, des ``def`` écrits en ``Def`` et pareil pour ``while``, ``if``, ``else`` ou ``for``). De même, l'*indentation* est cruciale en Python,
- Vous devez être plus *attentifs aux consignes de l'énoncé* (certains élèves oublient de donner la complexité dans les dernières questions),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller" des points* là où vous pouvez (la fin du sujet était pas trop dure ici, de même la partie sur la transformée de Fourier rapide commençait par des questions faciles),
- Attention au nom des variables et des fonctions. Par exemple :math:`\alpha` est à évité, préférez ``a`` ou ``alpha``, et ``moyenne_optimisee`` doit s'écrire avec un ``_`` au milieu (c'est un détail, mais forcez-vous à toujours à suivre *exactement les conventions de nommage de l'énoncé*),
- Lorsque le sujet demande une fonction "optimisée" (``moyenne_optimisee``, ``ecarttype_optimise``), doutez-vous bien que la complexité attendue doit être BIEN meilleure que la version naïve (ici, en :math:`\mathcal{O}(1)` au lieu de :math:`\mathcal{O}(n)`),
- Seulement quelques questions demandaient un *calcul de complexité*, il faut les faire ! C'est toujours des points faciles,
- La fin (question 12) contenant une *requête SQL*. Vous êtes peu nombreux à avoir eu les 2 points, mais c'était facile d'avoir 1.5 points (et c'est très rapide à écrire, encore des points gagnés),
- Dans un calcul de complexité, faites attention aux fonctions appellées (``frequence`` par exemple est en :math:`\mathcal{O}(N^2)` mais uniquement parce que ``fourier`` est en :math:`\mathcal{O}(N^2)`),
- Si le sujet impose un nom pour une constante, *utilisez CE nom* (``omegac``, ``alarme``).

--------------------------------------------------------------------------------

Documentation
-------------

.. automodule:: Afficher_un_histogramme_de_notes_DS4
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python Afficher_un_histogramme_de_notes_DS4.py

Le fichier Python se trouve ici : :download:`Afficher_un_histogramme_de_notes_DS4.py`.
