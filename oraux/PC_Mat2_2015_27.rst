`PC_Mat2_2015_27 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PC-Mat2-2015-27.pdf>`_
---------------------------------------------------------------------------------------------------------------

- *Thème :* simulations probabilistes, calcul asymptotique.

Documentation
^^^^^^^^^^^^^

.. automodule:: PC_Mat2_2015_27
   :members:
   :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^

.. runblock:: console

   $ python PC_Mat2_2015_27.py

Le fichier Python se trouve ici : :download:`PC_Mat2_2015_27.py`.
