Exemple 1 : Le labyrinthe exemple donne par le TP est :
[[0 1 0 0 0 0]
 [0 1 1 1 1 0]
 [0 1 0 1 0 0]
 [0 1 0 1 1 0]
 [0 1 1 0 1 0]
 [0 0 0 0 1 0]]
 ==> Et il admet une solution car : parcoursLabyrinthe(laby) = True
Exemple 2 : ce labyrinthe n'a pas de solution :
[[0 1 0 0 0 0]
 [0 1 1 1 1 0]
 [0 0 1 0 1 0]
 [0 1 1 1 1 0]
 [0 1 1 0 0 0]
 [0 0 0 0 0 0]]
 ==> En effet parcoursLabyrinthe(laby2) = False
==> On a fait quelques simulations, apparemment p_c = 0.6 a 0.01 pres.
Le graphe exemple de l'enonce est :
[[0 1 1 0 1 0 0 0 0]
 [1 0 0 0 1 1 0 0 0]
 [1 0 0 0 0 0 0 0 0]
 [0 0 0 0 0 0 0 1 1]
 [1 1 0 0 0 0 1 0 0]
 [0 1 0 0 0 0 1 0 0]
 [0 0 0 0 1 1 0 0 0]
 [0 0 0 1 0 0 0 0 0]
 [0 0 0 1 0 0 0 0 0]]
Pour le graphe g :
[[0 1 1 0 1 0 0 0 0]
 [1 0 0 0 1 1 0 0 0]
 [1 0 0 0 0 0 0 0 0]
 [0 0 0 0 0 0 0 1 1]
 [1 1 0 0 0 0 1 0 0]
 [0 1 0 0 0 0 1 0 0]
 [0 0 0 0 1 1 0 0 0]
 [0 0 0 1 0 0 0 0 0]
 [0 0 0 1 0 0 0 0 0]]
Existe - t-il un chemin entre j = 0 = A et k = G = 6 ? True (oui)
Existe - t-il un chemin entre j = 0 = A et k = D = 3 ? False (non)
Pour le graphe g :
[[0 1 1 0 1 0 0 0 0]
 [1 0 0 0 1 1 0 0 0]
 [1 0 0 0 0 0 0 0 0]
 [0 0 0 0 0 0 0 1 1]
 [1 1 0 0 0 0 1 0 0]
 [0 1 0 0 0 0 1 0 0]
 [0 0 0 0 1 1 0 0 0]
 [0 0 0 1 0 0 0 0 0]
 [0 0 0 1 0 0 0 0 0]]
Existe-t-il un chemin entre j = 0 = A et k = G = 6 ?
Oui, le voici : [0, 4, 6]
   ([A, B, E, G] ou [A, E, G] ou [A, B, F, G] soit [0, 1, 4, 6] ou [0, 4, 6] ou [0, 1, 6])
Existe-t-il un chemin entre j = 0 = A et k = D = 3 ? False (non)
Help on module TP3:

NAME
    TP3 - TP3 au Lycée Lakanal (sujet rédigé par Arnaud Basson).

FILE
    /home/lilian/teach/info-mp-2015-2016/TPs/TP3.py

DESCRIPTION
    - *Date :* jeudi 15-10 et 05-11 (2015),
    - *Auteur :* Lilian Besson, pour le cours d'informatique pour tous en prépa MP (http://perso.crans.org/besson/infoMP/),
    - *Licence :* MIT Licence (http://lbesson.mit-license.org).

FUNCTIONS
    afficheMilieuPoreux(milieuPoreux, title=None, filename=None)
        Affiche le milieu poreux et l'écoulement d'eau comme dans le TP.
        
        - On peut fournir le titre et le nom du fichier vers lequel on sauvegarde l'image.
    
    array(...)
        array(object, dtype=None, copy=True, order=None, subok=False, ndmin=0)
        
        Create an array.
        
        Parameters
        ----------
        object : array_like
            An array, any object exposing the array interface, an
            object whose __array__ method returns an array, or any
            (nested) sequence.
        dtype : data-type, optional
            The desired data-type for the array.  If not given, then
            the type will be determined as the minimum type required
            to hold the objects in the sequence.  This argument can only
            be used to 'upcast' the array.  For downcasting, use the
            .astype(t) method.
        copy : bool, optional
            If true (default), then the object is copied.  Otherwise, a copy
            will only be made if __array__ returns a copy, if obj is a
            nested sequence, or if a copy is needed to satisfy any of the other
            requirements (`dtype`, `order`, etc.).
        order : {'C', 'F', 'A'}, optional
            Specify the order of the array.  If order is 'C', then the array
            will be in C-contiguous order (last-index varies the fastest).
            If order is 'F', then the returned array will be in
            Fortran-contiguous order (first-index varies the fastest).
            If order is 'A' (default), then the returned array may be
            in any order (either C-, Fortran-contiguous, or even discontiguous),
            unless a copy is required, in which case it will be C-contiguous.
        subok : bool, optional
            If True, then sub-classes will be passed-through, otherwise
            the returned array will be forced to be a base-class array (default).
        ndmin : int, optional
            Specifies the minimum number of dimensions that the resulting
            array should have.  Ones will be pre-pended to the shape as
            needed to meet this requirement.
        
        Returns
        -------
        out : ndarray
            An array object satisfying the specified requirements.
        
        See Also
        --------
        empty, empty_like, zeros, zeros_like, ones, ones_like, fill
        
        Examples
        --------
        >>> np.array([1, 2, 3])
        array([1, 2, 3])
        
        Upcasting:
        
        >>> np.array([1, 2, 3.0])
        array([ 1.,  2.,  3.])
        
        More than one dimension:
        
        >>> np.array([[1, 2], [3, 4]])
        array([[1, 2],
               [3, 4]])
        
        Minimum dimensions 2:
        
        >>> np.array([1, 2, 3], ndmin=2)
        array([[1, 2, 3]])
        
        Type provided:
        
        >>> np.array([1, 2, 3], dtype=complex)
        array([ 1.+0.j,  2.+0.j,  3.+0.j])
        
        Data-type consisting of more than one element:
        
        >>> x = np.array([(1,2),(3,4)],dtype=[('a','<i4'),('b','<i4')])
        >>> x['a']
        array([1, 3])
        
        Creating an array from sub-classes:
        
        >>> np.array(np.mat('1 2; 3 4'))
        array([[1, 2],
               [3, 4]])
        
        >>> np.array(np.mat('1 2; 3 4'), subok=True)
        matrix([[1, 2],
                [3, 4]])
    
    cheminGraphe(A, j, k)
        Parcours (*en largeur*) du graphe A, en partant de j, et tentant d'atteindre k, renvoyant un chemin valable s'il existe, False sinon.
        
        - Mémoire :math:`O(n)`, temps max :math:`O(n^2)` pour n le nombre de sommets.
    
    creerMilieu(n, p)
        Créé un labyrinthe **aléatoire** de taille (n, n) représentant un milieu de porosité p.
    
    parcoursGraphe(A, j, k)
        Parcours (*en largeur*) du graphe A, en partant de j, et tentant d'atteindre k, renvoie True ou False selon qu'un chemin existe entre j et k dans A.
        
        - Mémoire :math:`O(n)`, temps max :math:`O(n^2)` pour n le nombre de sommets.
    
    parcoursLabyrinthe(laby, full=False)
        Parcours le labyrinthe laby avec l'algorithme détaillé dans le TP.
        
        - :math:`O(n^2)` en espace additionnel (dejavues prend :math:`n^2` et p prend au pire :math:`O(n^2)` pour graphe complet),
        - :math:`O(n^2)` en temps (chaque case ne peut être empilée et dépilée qu'une seule fois).
        - Si full est ``True``, on renvoie aussi le tableau des cases visitées (dejavues).
    
    simulationEcoulement(n, p, nb=20)
        Lance nb (20) simulations pour n, p et affiche le nombre de fois où l'eau s'est écoulée jusqu'en bas.
    
    verifierChemin(A, chemin)
        Vérifie que le chemin donné par la liste des sommets successifs (chemin) est bien un chemin valable pour le graphe A.
        
        - :math:`O(1)` en espace, :math:`O(\text{Nb chemin})` en temps.
    
    zeros(...)
        zeros(shape, dtype=float, order='C')
        
        Return a new array of given shape and type, filled with zeros.
        
        Parameters
        ----------
        shape : int or sequence of ints
            Shape of the new array, e.g., ``(2, 3)`` or ``2``.
        dtype : data-type, optional
            The desired data-type for the array, e.g., `numpy.int8`.  Default is
            `numpy.float64`.
        order : {'C', 'F'}, optional
            Whether to store multidimensional data in C- or Fortran-contiguous
            (row- or column-wise) order in memory.
        
        Returns
        -------
        out : ndarray
            Array of zeros with the given shape, dtype, and order.
        
        See Also
        --------
        zeros_like : Return an array of zeros with shape and type of input.
        ones_like : Return an array of ones with shape and type of input.
        empty_like : Return an empty array with shape and type of input.
        ones : Return a new array setting values to one.
        empty : Return a new uninitialized array.
        
        Examples
        --------
        >>> np.zeros(5)
        array([ 0.,  0.,  0.,  0.,  0.])
        
        >>> np.zeros((5,), dtype=np.int)
        array([0, 0, 0, 0, 0])
        
        >>> np.zeros((2, 1))
        array([[ 0.],
               [ 0.]])
        
        >>> s = (2,2)
        >>> np.zeros(s)
        array([[ 0.,  0.],
               [ 0.,  0.]])
        
        >>> np.zeros((2,), dtype=[('x', 'i4'), ('y', 'i4')]) # custom dtype
        array([(0, 0), (0, 0)],
              dtype=[('x', '<i4'), ('y', '<i4')])

DATA
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    g = [[0, 1, 1, 0, 1, 0, 0, 0, 0], [1, 0, 0, 0, 1, 1, 0, 0, 0], [1, 0, ...
    i = 5
    j = 6
    k = 8
    laby = [[0, 1, 0, 0, 0, 0], [0, 1, 1, 1, 1, 0], [0, 1, 0, 1, 0, 0], [0...
    laby2 = [[0, 1, 0, 0, 0, 0], [0, 1, 1, 1, 1, 0], [0, 0, 1, 0, 1, 0], [...
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


