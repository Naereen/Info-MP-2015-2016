`TP1 : Remise en forme <../TP1.pdf>`_
-------------------------------------
Premier TP, remise en forme sur de petites choses.

Documentation
^^^^^^^^^^^^^
.. automodule:: TP1
   :members:
   :undoc-members:
   :show-inheritance:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^
.. runblock:: console

   $ python TP1.py

Le fichier Python se trouve ici : :download:`TP1.py`.
