Cadeau de Noël !
----------------
.. image:: sapinNoel.gif
   :align: center
   :scale: 100%
   :alt:   Un sapin de Noel clignotant...
   :target: _images/sapinNoel.gif


Petit script Python pour tracer un sapin de Noël.
Pour joliment conclure `le dernier TP avant les vacances de Noël <TP4.html>`_...

--------------------------------------------------------------------------------

Documentation
^^^^^^^^^^^^^
.. automodule:: sapinNoel
   :members:
   :undoc-members:
   :show-inheritance:

--------------------------------------------------------------------------------

Gif ou MP4 ?
^^^^^^^^^^^^
L'animation, sauvegardée en image animée (``.gif``) :

.. image:: sapinNoel.gif


`L'animation, sauvegardée en film <_images/sapinNoel.mp4>`_ (``.mp4``).
