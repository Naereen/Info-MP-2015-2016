************* Module TP5
W: 30, 9: Redefining name 'l' from outer scope (line 24) (redefined-outer-name)
W: 30,17: Redefining name 'g' from outer scope (line 27) (redefined-outer-name)
W:174, 4: Redefining name 'thetas' from outer scope (line 260) (redefined-outer-name)
W: 90,19: Redefining name 'theta0' from outer scope (line 75) (redefined-outer-name)
W: 90,27: Redefining name 'thetaPoint0' from outer scope (line 78) (redefined-outer-name)
W: 90,10: Redefining name 'h' from outer scope (line 87) (redefined-outer-name)
W: 90,13: Redefining name 'tmax' from outer scope (line 81) (redefined-outer-name)
W:189,21: Redefining name 'l' from outer scope (line 24) (redefined-outer-name)
W:189,29: Redefining name 'g' from outer scope (line 27) (redefined-outer-name)
W:206,17: Redefining name 'listeTemps' from outer scope (line 262) (redefined-outer-name)
W:206,36: Redefining name 'methode' from outer scope (line 72) (redefined-outer-name)
W:228,18: Redefining name 'thetas' from outer scope (line 260) (redefined-outer-name)
W:228,26: Redefining name 'methode' from outer scope (line 72) (redefined-outer-name)
W:368, 4: Redefining name 'thetas' from outer scope (line 260) (redefined-outer-name)
W:277,25: Redefining name 'theta0' from outer scope (line 75) (redefined-outer-name)
W:277,33: Redefining name 'thetaPoint0' from outer scope (line 78) (redefined-outer-name)
W:277,16: Redefining name 'h' from outer scope (line 87) (redefined-outer-name)
W:277,19: Redefining name 'tmax' from outer scope (line 81) (redefined-outer-name)
W:407, 9: Unused argument 't' (unused-argument)
W:494, 8: Redefining name 'thetas' from outer scope (line 260) (redefined-outer-name)
W:480, 4: Redefining name 'theta0' from outer scope (line 75) (redefined-outer-name)
W:481, 4: Redefining name 'nbPoints' from outer scope (line 84) (redefined-outer-name)
W:477, 4: Redefining name 'g' from outer scope (line 27) (redefined-outer-name)
W:451,36: Redefining name 'tmax' from outer scope (line 81) (redefined-outer-name)
W:478, 4: Redefining name 'l' from outer scope (line 24) (redefined-outer-name)
W:490, 4: Redefining name 'methode' from outer scope (line 72) (redefined-outer-name)
W:482, 4: Redefining name 'listeTemps' from outer scope (line 262) (redefined-outer-name)
W:518,13: Redefining name 'h' from outer scope (line 87) (redefined-outer-name)
W:518,16: Redefining name 'thetas' from outer scope (line 260) (redefined-outer-name)
W:557,21: Redefining name 'thetaPoint0' from outer scope (line 78) (redefined-outer-name)
W:557,13: Redefining name 'theta0' from outer scope (line 75) (redefined-outer-name)
W:557,42: Redefining name 'g' from outer scope (line 27) (redefined-outer-name)
W:557,34: Redefining name 'l' from outer scope (line 24) (redefined-outer-name)
W:658, 8: Redefining name 'thetaPoint0' from outer scope (line 78) (redefined-outer-name)
W:656, 4: Redefining name 'theta0' from outer scope (line 75) (redefined-outer-name)
W:668, 4: Redefining name 'h' from outer scope (line 87) (redefined-outer-name)
W:667, 4: Redefining name 'tmax' from outer scope (line 81) (redefined-outer-name)


Report
======
221 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |1          |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|method   |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|function |14     |14         |=          |100.00      |42.86    |
+---------+-------+-----------+-----------+------------+---------+



External dependencies
---------------------
::

    matplotlib 
      \-animation (TP5)
      \-pyplot (TP5)
    numpy (TP5)
    scipy 
      \-integrate (TP5)



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |222    |31.40 |222      |=          |
+----------+-------+------+---------+-----------+
|docstring |387    |54.74 |386      |+1.00      |
+----------+-------+------+---------+-----------+
|comment   |28     |3.96  |27       |+1.00      |
+----------+-------+------+---------+-----------+
|empty     |70     |9.90  |70       |=          |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |0        |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |0.000    |=          |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |0      |1        |-1.00      |
+-----------+-------+---------+-----------+
|refactor   |0      |0        |=          |
+-----------+-------+---------+-----------+
|warning    |37     |38       |-1.00      |
+-----------+-------+---------+-----------+
|error      |0      |0        |=          |
+-----------+-------+---------+-----------+



Messages
--------

+---------------------+------------+
|message id           |occurrences |
+=====================+============+
|redefined-outer-name |36          |
+---------------------+------------+
|unused-argument      |1           |
+---------------------+------------+



Global evaluation
-----------------
Your code has been rated at 8.33/10 (previous run: 8.24/10, +0.09)
That's pretty good. Good work mate.

